<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="style.css" />
  <title>Base date</title>
</head>

<body>
  <div class="container">
    <?php
    $server = "X";
    $login = "X";
    $password = "X";
    $bd = "X";

    $conn = new mysqli($server, $login, $password, $bd);

    if ($conn->connect_error) {
      die("Ошибка: " . $conn->connect_error);
    }
    $sql = "SELECT * FROM students";
    if ($result = $conn->query($sql)) {
      $rowsCount = $result->num_rows; // количество полученных строк
      echo "<p>Получено объектов: $rowsCount</p>";
      echo "
      <table >
        <tr>
          <th>Фамилия</th> 
          <th>Имя</th>
          <th>Отчество</th>
          <th>Возраст</th>
          <th>Дата рождения</th>
        </tr>";
      foreach ($result as $row) {
        echo "<tr>";
        echo "<td>" . $row["surname"] . "</td>";
        echo "<td>" . $row["name"] . "</td>";
        echo "<td>" . $row["thirdname"] . "</td>";
        echo "<td>" . $row["age"] . "</td>";
        echo "<td>" . $row["bdate"] . "</td>";
        echo "</tr>";
      }
      echo "</table>";
      $result->free();
    } else {
      echo "Ошибка: " . $conn->error;
    }
    $conn->close();
    ?>
    <button class="button"><a href="../index.html">Назад</a></button>
  </div>
</body>

</html>